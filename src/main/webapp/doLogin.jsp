<%-- 
    Document   : doLogin
    Created on : Mar 29, 2018, 3:03:07 PM
    Author     : Amir As-Sarawakiyy
--%>
<%@page import="com.onelogin.saml2.Auth"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
<!--        <h1>Hello World!</h1>-->
        <%
		Auth auth = new Auth(request, response);
		if (request.getParameter("attrs") == null) {
			auth.login();
		} else {
			String x = request.getPathInfo();
			auth.login("/java-saml-tookit-jspsample/attrs.jsp");
		}
	%>
    </body>
</html>
