FROM library/tomcat:latest AS base
EXPOSE 8080

FROM library/maven:3-jdk-8 AS build
WORKDIR /vento
COPY pom.xml ./
COPY src src
RUN mvn package -DskipTests

FROM base AS publish
COPY --from=build /vento/target/*.war /usr/local/tomcat/webapps/ROOT.war
COPY conf/ /usr/local/tomcat/conf/
RUN rm -rf /usr/local/tomcat/webapps/ROOT
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]
